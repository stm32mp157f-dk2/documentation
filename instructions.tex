\documentclass[10pt]{article}
\usepackage{CJKutf8}
\usepackage{hyperref}
\usepackage{graphicx}
%\usepackage{listings}
\usepackage{solarized-light}

% making the text of the report sans serif
\renewcommand{\familydefault}{\sfdefault}

\title{Setting up and using STM32MP157F development kit board \\[1ex] \large \begin{CJK}{UTF8}{min}南山大学\end{CJK}}
\date{}
\author{Vincent Conus}

\begin{document}
 
\maketitle
% \tableofcontents

\begin{figure}[h]
  \centering
  \includegraphics[width=0.5\textwidth]{./img/board.png}
\end{figure}


%--------------------------------------------------------------------------------------
\section{Introduction and motivation}
The \href{https://www.st.com/en/evaluation-tools/stm32mp157f-dk2.html}{STM32MP157F-DK2} is an good demonstration board for a heterogeneous cores system, as the SoC integrates both a Cortex-A7 and a Cortex-M4, providing an integrated system that can run both a general-purpose OS (Linux) as well as a real-time system.\\

Whoever, the setup, usage and development on of such less common architecture can be doubting and the documentation of ST themselves is split between many guides and pages, making it hard to follow as a guide.\\

The goal of this document is for one to be able to take a step-by-step approach for setting up and using this board.\\


%--------------------------------------------------------------------------------------
\section{Development Kit information and tools}

\subsection{Tools}
Here are the tools and devices required to follow this guide:

\begin{itemize}
\item The STM32MP157F-DK2 is the development kit (called dev kit or DK from here on) on which this guide was based on. The general information provided here might work similarly on other STM32MP1 family boards. The board is expected to be running the default, demo program off the SD-card Linux distribution.
\item A SD-card, as stated above.
\item A USB-C power supply. 20W is recommended.
\item A USB-C cable for Ethernet-over-USB connection.
\item (optional) A micro-USB cable, for the serial connection.
\item A computer running Linux (MacOS might work similarly). The distribution-specific commands for this guide are given for a Debian-based distribution (\verb|apt-get| package manager).\\
  The connection with the dev kit takes up to two (2) USB ports, when using the IDE debugger. Otherwise, a single port can be used for either serial connection or Ethernet-over-USB.\\
  If these are not an option, it should be possible to connect to the board using SSH over standard Ethernet connection, since the board has one. This has not been tested yet.
\item An internet connection for that computer is preferred, for pulling git repositories and installing dependencies.
\end{itemize}

The required software / packages / dependencies will be presented along the guide, at the moment they are specifically required. However, it looks like the CubeIDE (or any other Eclipse-like IDE) is required, at the very least for the first makefile generation and build of the project. This last point will be details later.


\subsection{Boot modes}
On the back of the board, a double switch labeled ``BOOT0 BOOT2'' can be seen.
As shown on the figure \ref{fig:boot_mode}, the combination of both switches lead to different boot mode.\\

For this guide we will be using the ``on on'' mode to boot on the SD card Linux distribution provided by ST. This distribution have the access that allow to run programs on the Cortex-M4 and see it's returns.

\pagebreak
\begin{figure}[h]
  \centering
  \includegraphics[width=0.4\textwidth]{./img/boot\_modes.png}
  \caption{All possible boot modes selectable with the switches}
  \label{fig:boot_mode}
\end{figure}


\subsection{Serial connection}
This approach is the easiest but also happen to be slower and less flexible.\\
A first test that can be made is to connect on the device via the serial port. When you plug the micro-USB cable to your PC, a device named \verb|/dev/ttyACM0| or similar (\verb|/dev/ttyACM1| p.ex.) should appear.\\
It is possible to use this interface to access a terminal on the board. Here are the commands to install the required program and use it:

\begin{lstlisting}[language=bash]
  sudo apt-get install minicom
  minicom -D /dev/ttyACM0
\end{lstlisting}

\subsection{SSH connection}
This is a more general approach to access a Linux remotely and can eventually also be used to transfer data from and to the board.The \verb|HostKeyAlgorithms| parameter is required for the connection to work, even from a script.\\

\begin{lstlisting}[language=bash]
  sudo apt-get install openssh
  ssh -o HostKeyAlgorithms=+ssh-rsa root@192.168.7.1 
\end{lstlisting}

\subsection{The on-board Linux}
The dev kit seems to be provide with an SD-card with ST special Linux distribution, running an example GTK application.
The figure \ref{fig:openst_linux} below shows a terminal connected with SSH to the DK and displaying some basic information about it.

\begin{figure}[h]
  \centering
  \includegraphics[width=1\textwidth]{./img/openst_linux.png}
  \caption{Basic information about the OpenST Linux running on the DK}
  \label{fig:openst_linux}
\end{figure}

% --------------------------------------------------------------------------------------
\pagebreak
\section{Cortex-M4}
The Cortex-M4 (M4 from now on) is a real-time capable micro-controller chip. It's programs must be ran bare-metal, or with a Real Time Operation System (RTOS).\\

The examples given by ST on their \href{https://wiki.st.com/stm32mpu-ecosystem-v3/wiki/Getting_started/STM32MP1_boards/STM32MP157x-DK2}{``Getting started'' page} are useful but the order presented for the operation is not very convenient to grasp one aspect of the board at the time.\\
Indeed, the first examples of a project to import specifically for the M4 is something that require openAMP to work on the Cortex-A7 (A7 from now on), which, as it will be described in detail in the next section, requires a recompilation of the Linux kernel to enable the RPMsg.\\

The first step here then is to create a minimal program to be run on the M4, then presenting a way to run something on FreeRTOS, and finally working with openAMP.

\subsection{``Getting started'' example project}
Some example projects are provided by ST, but their structure and dependencies are complex.
A ``portable'' version of the project can be found \href{github.com/STMicroelectronics/STM32CubeMP1}{in the official repository}. In particular there is a specific example that is particularly interesting to us: the \href{github.com/STMicroelectronics/STM32CubeMP1/tree/master/Projects/STM32MP157C-DK2/Applications/OpenAMP/OpenAMP\_FreeRTOS\_echo}{OpenAMP\_FreeRTOS\_echo}, as it already includes a working version of both FreeRTOS and OpenAMP.\\

The whole firmware package can be cloned as follow:

\begin{lstlisting}[language=bash]
  cd Download
  git clone https://github.com/STMicroelectronics/STM32CubeMP1.git
\end{lstlisting}

\subsection{Imported project and IDE}
This step requires the \href{https://www.st.com/en/development-tools/stm32cubeide.html}{STM32 Cube IDE} to be installed.
Indeed, this tool is required, as the firmware was pulled, to generate the links to the headers and the makefile.

\begin{itemize}
\item Install the IDE on you system following the instruction provided by ST.
\item In \verb|File, Import|, you can import ``Existing project into Workspace''.
\item In the structure of the imported repository, you can search for a ``STM32CubeIDE'' directory in the project you want to import (here, ``OpenAMP\_FreeRTOS\_echo'')and import it.
\end{itemize}

Once all these steps has been taken, you must view something similar to what is on the figure \ref{fig:ide_imported} below.

\begin{figure}[h]
  \centering
  \includegraphics[width=1\textwidth]{./img/ide_imported.png}
  \caption{The STM32CubeIDE after the template project has been imported and built}
  \label{fig:ide_imported}
\end{figure}

Now the project has been imported, it is needed to build it at least once from the IDE, so it can generate the makefile.
As the IDE is derived from Eclipse, it uses it's tools to generate a makefile form the project structure provided.


\subsection{Building the example project from the command line}
Once the project has been imported and the first makefile has been generated, it become possible to rebuild the project by hand.\\
It turns out that a specific cross-compiler for ARM is required, which can be installed as follow:


\begin{lstlisting}[language=bash]
  sudo apt-get install gcc-arm-none-eabi
\end{lstlisting}

Then, by going in the ``Debug'' dir of the project, one can simply run these commands to respectively build and clean the whole project, with respectively:
`
\begin{lstlisting}[language=bash]
  make all
  make clean
\end{lstlisting}

The result is a binary \verb|.elf| file that can be used as the firmware for the M4.

\subsection{Uploading a project on the target}
Now a binary for the M4 has been build, we can run it on the hardware.
For this to be made, the binary should be loaded in the MCU SRAM. The most standard way to do so happens through the Linux system running on the A7.\\
The shared memory part to access the M4 appears as a system device named \verb|/sys/class/remoteproc/remoteproc0/|, and the dirs and files inside are used for various tasks, like uploading a new firmware, starting and stopping the run of the core.\\

There are two bash scripts that are important in order to accomplish the task smoothly.\\
Firstly, the \verb|fw_cortex_m4.sh| script that can be find in all imported project for the DK. When given the ``correct'' directory structures and files, this script can deploy the binary firmware on the M4.\\
The second important script is the \verb|deploy.sh|, that can be find in my repository (++ WORK IN PROGRESS++ my repository need to be setup correctly with a copy of the example STM32 projects and the others files / modification I made, including this deploy.sh script). This works as both a reminder and a script to clean, rebuild, prepare the structure, upload and load the firmware on the DK using a SSH connection.\\
One can simply run the script as follow:

\begin{lstlisting}[language=bash]
  bash deploy.sh
\end{lstlisting}

\subsection{M4 debugging \& printf}
Even working from the ST IDE, it is not convenient to use the debug tools to know what is happening with the M4 firmware.\\

Fortunately, it is possible to see the ``printf'' returns for the A7 Linux, by checking the \verb|/sys/kernel/debug/remoteproc/remoteproc0/trace0|. It is not the most convenient way since it requires a connection to the A, but with a command like below on the host machine, it is possible to have an almost live view of the returns of the M4 firmware.

\begin{lstlisting}[language=bash]
  watch -n 0.2 ssh -o HostKeyAlgorithms=+ssh-rsa root@192.168.7.1 "cat /sys/kernel/debug/remoteproc/remoteproc0/trace0"
\end{lstlisting}

\subsection{openAMP for Cortex-M4}
One of the main goal of this whole procedure was to be able to use OpenAMP between the A7 and the M4. Even though the M4 is now ready, some extra steps are required on the A7 Linux side for this example to work completely.



% --------------------------------------------------------------------------------------
\pagebreak
\section{++WORK IN PROGRESS++ Cortex-A}
The Cortex-A7 is a microprocessor unit capable of running a Linux distribution. This system must
be able to talk with the Cortex-M through a device interface.

\subsection{Default SD card image}
With the boot mode set to ``SD card'', the board can boot a Linux image running an example GTK application.
By default, this image does not has the proper kernel modules required to use RPMsg and to communicate with the Cortex-M.
This will be presented later on.


\subsection{C and python on Cortex-A}
Because a full Linux distribution runs on the Cortex-A core, it is possible to compile and run C code, and also to interpreter Python. However, it is not convenient to work through serial or SSH for code editing, and the compiling of bigger code can be very slow.\\
The solution is to cross-compile, similarly to what is happening for the Cortex-M, on you host machine and to upload the executable on the target.

\subsection{Kernel recompilation}

\subsection{++WORK IN PROGRESS++ openAMP}



\begin{lstlisting}[language=bash]
  # try this one for loadig the module for rpmsg
  modprobe rpmsg_user_dev_driver


  # for kernel compilation
  sudo apt-get install libssl-dev gcc-arm-linux-gnueabi u-boot-tools

  make -j16 ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- uImage vmlinux dtbs LOADADDR=0xC2000040
  make -j16 ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- modules
  make -j16 ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- INSTALL_MOD_PATH="$PWD/install_artifact" modules_install
\end{lstlisting}


\subsection{++WORK IN PROGRESS++ FreeRTOS}
There is another example project that can be used as is to be ran on the Cortex-M, and it turns out to be something we will also want to use later.\\
Indeed, it is possible to run FreeRTOS as an example, and this systems is most likely to be useful for us in the ROS setup.


%--------------------------------------------------------------------------------------
\section{RPMsg}

%--------------------------------------------------------------------------------------
\section{Conclusion}
 
\end{document}